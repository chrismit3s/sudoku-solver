import numpy as np
from platform import system
from ctypes import cdll, Structure, POINTER
from ctypes import c_bool, c_char_p, c_uint8, c_uint16


class SolveSequence(Structure):
    _fields_ = [
        ("x", c_uint8),
        ("y", c_uint8),
        ("value", c_uint16),
    ]


if "_backtracker" not in dir():
    _backtracker = cdll.LoadLibrary(r"out\backtracker.dll" if system() == "Windows" else "out/backtracker.so")
    _backtracker.solve.restype = POINTER(SolveSequence)
    _backtracker.solve.argtypes = (c_char_p, c_bool)


def backtrack(grid, colored, alphabet):
    alphabet = sorted(alphabet)

    # lookup to convert from alphabet (chars) to 1-9 (ints)
    lookup = {c: ord("1") + i for i, c in enumerate(alphabet)}
    lookup[None] = ord("0")

    # generate gird string and count missing cells
    grid_str = bytes(lookup[cell.value] for _, cell in np.ndenumerate(grid))
    missing_cells = grid_str.count(ord("0"))

    # call c backtracking function, convert and free array
    solve_sequence = _backtracker.solve(grid_str, colored)
    if solve_sequence:
        ret = [((s.x, s.y), alphabet[s.value - 1]) for s in solve_sequence[:missing_cells]]
        _backtracker.free_solve_seq(solve_sequence)
        return ret
    else:
        print("SOLVING FAILED")
