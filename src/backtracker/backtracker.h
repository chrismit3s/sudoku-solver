#ifndef BACKTRACKER_H
#define BACKTRACKER_H

#include<stdbool.h>

//#define DEBUG

// return values of cell_check
#define CONTRADICTION  (-1)
#define VALUE_KNOWN    (-2)
#define VALUE_FOUND    (0)

// 9 '1's, 1 '0' as no cell will have a value of 0 (0b11_1111_1110)
#define FULL ~((unsigned)~0 << 10 | 1)

// classic bit ops
#define GET_BIT(x,n)      (((x) >> (n)) & 1)
#define SET_BIT(x,n)      (x |= 1 << (n))
#define CLEAR_BIT(x,n)    (x &= ~(1 << (n)))
#define SET_BIT_TO(x,n,v) ((v) ? SET_BIT(x,n) : (CLEAR_BIT(x,n)))


typedef unsigned char bitvec8_t;
typedef unsigned short bitvec16_t;
typedef unsigned long bitvec32_t;

typedef unsigned char index_t;
typedef unsigned short value_t;

typedef struct cell_t {
	unsigned int value_known:1;
	value_t value:10;  // 0th bit unused, just to that bit 1 means 1 is a possible value
} cell_t;

typedef struct solve_seq_t {
	index_t x;
	index_t y;
	value_t value;
} solve_seq_t;


void free_solve_seq(solve_seq_t*);
solve_seq_t* solve(const char*, bool);


#endif /* BACKTRACKER_H */
