def rprint(*out_strs, sep=" ", end="\n", **kwargs):
    print_str = sep.join(str(s) for s in out_strs)
    to_fill = rprint._len_last_print - len(print_str)
    print(print_str + " " * to_fill, end=end, **kwargs)
    rprint._len_last_print = (len(print_str.rstrip())
                              if "\r" in end
                              else 0)
rprint._len_last_print = 0  # noqa: E305
